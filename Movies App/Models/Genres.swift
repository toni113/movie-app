//
//  Genres.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 20.2.22.
//

import Foundation

class Genres: Codable {
    
    var id: Int?
    var name: String?
    
}
