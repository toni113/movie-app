//
//  Pagination.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import Foundation

struct Pagination: Codable {
    
    var total_pages: Int?
    var total_results: Int?
    var page: Int?
    var movies: [Movie]?
    
    enum CodingKeys: String, CodingKey {
        case movies = "results"
        case total_pages = "total_pages"
        case total_results = "total_results"
        case page = "page"
    }
    
}
