//
//  Video.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import Foundation

class Video: Codable {
    
    var iso_639_1: String?
    var iso_3166_1: String?
    var name: String?
    var key: String?
    var site: String?
    var size: Int?
    var type: String?
    var official: Bool?
    var published_at: String?
    var id: String?
    
}
