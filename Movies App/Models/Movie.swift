//
//  Movie.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import Foundation

class Movie: Codable {
    
    var id: Int?
    var adult: Bool?
    var backdrop_path: String?
    var original_language: String?
    var original_title: String?
    var overview: String?
    var poster_path: String?
    var release_date: String?
    var title: String?
    var video: Bool?
    var vote_average: Double?
    var vote_count: Int?
    var popularity: Double?
    var genre_ids: [Int]?
    var genres: [Genres]?
    var homepage: String?
    var budget: Int?
    var imdb_id: String?
    var production_companies: [ProductionCompany]?
    var revenue: Int?
    var tagline: String?
    var status: String?
    
}
