//
//  ProductionCompany.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import Foundation

class ProductionCompany: Codable {
    
    var id: Int?
    var logo_path: String?
    var name: String?
    var origin_country: String?
    
}
