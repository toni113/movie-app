//
//  SearchMoviewViewController.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 20.2.22.
//

import UIKit

class SearchMoviewViewController: UIViewController {
    
    var searchBar: UISearchBar!
    var buttonsHolderView: ButtonsHolderView!
    var collectionView: UICollectionView!
    var pagination = Pagination()
    var paginationTvshows = Pagination()
    var movies = [Movie]()
    var tvShows = [Movie]()
    
    //Helpers
    var numberOfCellsHorizontal: CGFloat = 2
    
    //Helpers
    var selectedButton: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getOrientation()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.view.backgroundColor = .systemGray6
        self.title = "Search Movies"
        
        searchBar = UISearchBar()
        searchBar.tag = 1
        searchBar.delegate = self
        searchBar.returnKeyType = .search
        searchBar.placeholder = "Search"
        searchBar.barTintColor = .white
        searchBar.searchBarStyle = .default
        searchBar.layer.cornerRadius = 10
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        searchBar.clipsToBounds = true
        searchBar.searchTextField.backgroundColor = .white
        
        buttonsHolderView = ButtonsHolderView()
        buttonsHolderView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "movieCell")
        
        self.view.addSubview(searchBar)
        self.view.addSubview(buttonsHolderView)
        self.view.addSubview(collectionView)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        searchBar.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top).offset(10)
            make.left.right.equalTo(self.view).inset(10)
            make.height.equalTo(40)
        }
        
        buttonsHolderView.snp.makeConstraints { make in
            make.top.equalTo(searchBar.snp.bottom)
            make.left.right.equalTo(self.view)
            make.height.equalTo(45)
        }
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(buttonsHolderView.snp.bottom)
            make.left.equalTo(self.view.safeAreaLayoutGuide.snp.left).inset(10)
            make.right.equalTo(self.view.safeAreaLayoutGuide.snp.right).inset(10)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    //MARK: - ORIENTATION CHANGED
    @objc func getOrientation() {
        if Utilities.sharedInstance.isLandscape() {
            numberOfCellsHorizontal = 4
        } else {
            if Utilities.sharedInstance.isIPad() {
                numberOfCellsHorizontal = 3
            } else {
                numberOfCellsHorizontal = 2
            }
        }
        
        if let collectionView = collectionView {
            collectionView.reloadData()
        }
    }
}

//MARK: - BUTTONS HOLDER VIEW DELEGATE
extension SearchMoviewViewController: ButtonsHolderViewDelegate {
    func searchButtonSelected(tag: Int) {
        if tag == 1 {
            self.title = "Search Movies"
        } else {
            self.title = "Search TV Shows"

        }
        
        self.searchBar.text = ""
        self.tvShows = [Movie]()
        self.movies = [Movie]()

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

//MARK: - COLLECTION VIEW DELEGATE AND DATASOURCE
extension SearchMoviewViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.delegate = self
        cell.setupCell(movie: movies[indexPath.row], index: indexPath.row)
        
        if selectedButton == 1 {
            if indexPath.row == movies.count - 1 {
                if pagination.total_pages ?? 0 > pagination.page ?? 0 {
                    searchMovies(searchString: searchBar.text ?? "", pageNumber: (pagination.page ?? 0) + 1)
                }
            }
        } else {
            if indexPath.row == tvShows.count - 1 {
                if paginationTvshows.total_pages ?? 0 > paginationTvshows.page ?? 0 {
                    searchTvShows(searchString: searchBar.text ?? "", pageNumber: (paginationTvshows.page ?? 0) + 1)
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let window = UIApplication.shared.windows.first
        let leftPadding: CGFloat = window?.safeAreaInsets.left ?? 0
        let rigthPadding: CGFloat = window?.safeAreaInsets.right ?? 0
        let safeAreaSize = leftPadding + rigthPadding
        let widht = (self.view.frame.size.width - (numberOfCellsHorizontal * 10) - 10 - safeAreaSize) / numberOfCellsHorizontal
        return CGSize(width: widht , height: widht * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailsViewController(movie: movies[indexPath.row]), animated: true)
    }
}

//MARK: - SEARCH BAR DELEGATE
extension SearchMoviewViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        searchBar.text = ""
        if selectedButton == 1 {
            movies = [Movie]()
        } else {
            tvShows = [Movie]()
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.resignFirstResponder()
        if selectedButton == 1 {
            self.movies = [Movie]()
            self.searchMovies(searchString: searchBar.text ?? "", pageNumber: 1)
        } else {
            self.tvShows = [Movie]()
            self.searchTvShows(searchString: searchBar.text ?? "", pageNumber: 1)
        }
    }
}

//MARK: - MOVIE COLLECTION VIEW CELL DELEGATE
extension SearchMoviewViewController: MovieCollectionViewCellDelegate {
    func favoriteButtonAction(sender: UIButton) {
        var favMovies = UserPersistence.sharedInstance.getFavoriteMovies()
        if favMovies.contains(where: {$0.id == movies[sender.tag].id}) {
            if let index = favMovies.firstIndex(where: {$0.id == movies[sender.tag].id}){
                favMovies.remove(at: index)
            }
            UserPersistence.sharedInstance.addToFavorite(favoriteMovies: favMovies)
        } else {
            favMovies.append(movies[sender.tag])
            UserPersistence.sharedInstance.addToFavorite(favoriteMovies: favMovies)
        }
        DispatchQueue.main.async {
            self.collectionView.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
        }
    }
}

//MARK: - API COMMUNICATION
extension SearchMoviewViewController {
    
    //MARK: - SEARCH MOVIES
    func searchMovies(searchString: String, pageNumber: Int) {
        let urlString = NetworkConstants.Enpodints.searchMovies + searchString + "&page=" + String(pageNumber)
        ApiManager.sharedInstance.fetchData(urlString: urlString) { (pagination: Pagination) in
            self.pagination = pagination
            for movie in pagination.movies ?? [] {
                self.movies.append(movie)
            }
            Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: false)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    //MARK: - SEARCH TV SHOWS
    func searchTvShows(searchString: String, pageNumber: Int) {
        let urlString = NetworkConstants.Enpodints.searchTvShows + searchString + "&page=" + String(pageNumber)
        ApiManager.sharedInstance.fetchData(urlString: urlString) { (pagination: Pagination) in
            self.paginationTvshows = pagination
            for movie in pagination.movies ?? [] {
                self.tvShows.append(movie)
            }
            Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: false)
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
}
