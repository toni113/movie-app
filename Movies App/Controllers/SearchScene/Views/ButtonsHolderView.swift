//
//  ButtonsHolderView.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import UIKit

protocol ButtonsHolderViewDelegate {
    func searchButtonSelected(tag: Int)
}

class ButtonsHolderView: UIView {
    
    var selectedButtonLineView: UIView!
    var moviesButton: UIButton!
    var tvShowsButton: UIButton!
    
    var delegate: ButtonsHolderViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.backgroundColor = .clear
        
        selectedButtonLineView = Utilities.sharedInstance.createView(color: .systemBlue, cornerRadius: 0)
        
        moviesButton = Utilities.sharedInstance.createButton(title: "Movies", backgorundColor: .clear, textColor: .black, imageName: "")
        moviesButton.tag = 1
        moviesButton.addTarget(self, action: #selector(buttonActions(sender:)), for: .touchUpInside)
        tvShowsButton = Utilities.sharedInstance.createButton(title: "TV Shows", backgorundColor: .clear, textColor: .black, imageName: "")
        tvShowsButton.tag = 2
        tvShowsButton.addTarget(self, action: #selector(buttonActions(sender:)), for: .touchUpInside)
        
        self.addSubview(selectedButtonLineView)
        self.addSubview(moviesButton)
        self.addSubview(tvShowsButton)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        selectedButtonLineView.snp.makeConstraints { make in
            make.bottom.equalTo(self)
            make.height.equalTo(1)
            make.left.right.equalTo(moviesButton)
        }
        
        moviesButton.snp.makeConstraints { make in
            make.left.top.equalTo(self)
            make.right.equalTo(self.snp.centerX)
            make.bottom.equalTo(selectedButtonLineView.snp.top)
        }
        
        tvShowsButton.snp.makeConstraints { make in
            make.right.top.equalTo(self)
            make.left.equalTo(self.snp.centerX)
            make.bottom.equalTo(selectedButtonLineView.snp.top)
        }
    }
    
    //MARK: - BUTTON ACTIONS
    @objc func buttonActions(sender: UIButton) {
        remakeConstraintsForLineView(sender: sender)
        delegate?.searchButtonSelected(tag: sender.tag)
    }
    
    //MARK: - REMAKE CONSTRAINTS FOR SELECTED BUTTON
    func remakeConstraintsForLineView(sender: UIButton) {
        UIView.animate(withDuration: 0.2) {
            self.selectedButtonLineView.snp.remakeConstraints { make in
                make.bottom.equalTo(self)
                make.height.equalTo(1)
                make.left.right.equalTo(sender)
            }
            self.layoutIfNeeded()
        }
    }
}
