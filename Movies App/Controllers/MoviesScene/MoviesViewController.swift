//
//  ViewController.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import UIKit
import SnapKit

class MoviesViewController: UIViewController {

    var moviesCollectionView: UICollectionView!
    var movies = [Movie]()
    var pagination = Pagination()
    
    //Helpers
    var numberOfCellsHorizontal: CGFloat = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getOrientation()
        setupViews()
        setupConstraints()
        Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: true)
        getTrendingMovies(pageNumber: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moviesCollectionView.reloadData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.view.backgroundColor = .systemGray6
        self.title = "Trending movies"
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        let searchBarButtonItem = createBarButtonItem(tag: 1, imageName: "search")
        let favoritesBarButtonItem = createBarButtonItem(tag: 2, imageName: "favorite")
        
        self.navigationItem.rightBarButtonItems = [favoritesBarButtonItem, searchBarButtonItem]
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        moviesCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        moviesCollectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        moviesCollectionView.backgroundColor = .clear
        moviesCollectionView.delegate = self
        moviesCollectionView.dataSource = self
        moviesCollectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "movieCell")
            
        self.view.addSubview(moviesCollectionView)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        moviesCollectionView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(self.view.safeAreaLayoutGuide.snp.left).inset(10)
            make.right.equalTo(self.view.safeAreaLayoutGuide.snp.right).inset(10)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
    }
    
    //MARK: - ORIENTATION CHANGED
    @objc func getOrientation() {
        if Utilities.sharedInstance.isLandscape() {
            numberOfCellsHorizontal = 4
        } else {
            if Utilities.sharedInstance.isIPad() {
                numberOfCellsHorizontal = 3
            } else {
                numberOfCellsHorizontal = 2
            }
        }
        
        if let collectionView = moviesCollectionView {
            collectionView.reloadData()
        }
    }
    
    //MARK: - CREATE BAR BUTTON ITEM
    func createBarButtonItem(tag: Int, imageName: String) -> UIBarButtonItem {
        let button = UIButton()
        button.setImage(UIImage(named: imageName), for: .normal)
        button.tag = tag
        button.addTarget(self, action: #selector(navigationButtonActions(sender:)), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        let currWidth = barButton.customView?.widthAnchor.constraint(equalToConstant: 38)
        currWidth?.isActive = true
        let currHeight = barButton.customView?.heightAnchor.constraint(equalToConstant: 38)
        currHeight?.isActive = true
        
        
        return barButton
    }
    
    //MARK: - NAVIGATION BUTTON ACTIONS
    @objc func navigationButtonActions(sender: UIBarButtonItem) {
        if sender.tag == 1 {
            self.navigationController?.pushViewController(SearchMoviewViewController(), animated: true)
        } else {
            self.navigationController?.pushViewController(FavoriteMoviesViewController(), animated: true)
        }
    }
}

//MARK: - COLLECTION VIEW DELEGATE AND DATASOURCE
extension MoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.delegate = self
        cell.setupCell(movie: movies[indexPath.row], index: indexPath.row)
        
        if indexPath.row == movies.count - 1 {
            if pagination.total_pages ?? 0 > pagination.page ?? 0 {
                getTrendingMovies(pageNumber: (pagination.page ?? 0) + 1)
            }
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let window = UIApplication.shared.windows.first
        let leftPadding: CGFloat = window?.safeAreaInsets.left ?? 0
        let rigthPadding: CGFloat = window?.safeAreaInsets.right ?? 0
        let safeAreaSize = leftPadding + rigthPadding
        let widht = (self.view.frame.size.width - (numberOfCellsHorizontal * 10) - 10 - safeAreaSize) / numberOfCellsHorizontal
        return CGSize(width: widht , height: widht * 1.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailsViewController(movie: movies[indexPath.row]), animated: true)
    }
}

//MARK: - MOVIE COLLECTION VIEW CELL DELEGATE
extension MoviesViewController: MovieCollectionViewCellDelegate {
    func favoriteButtonAction(sender: UIButton) {
        var favMovies = UserPersistence.sharedInstance.getFavoriteMovies()
        if favMovies.contains(where: {$0.id == movies[sender.tag].id}) {
            if let index = favMovies.firstIndex(where: {$0.id == movies[sender.tag].id}){
                favMovies.remove(at: index)
            }
            UserPersistence.sharedInstance.addToFavorite(favoriteMovies: favMovies)
        } else {
            favMovies.append(movies[sender.tag])
            UserPersistence.sharedInstance.addToFavorite(favoriteMovies: favMovies)
        }
        DispatchQueue.main.async {
            self.moviesCollectionView.reloadItems(at: [IndexPath(item: sender.tag, section: 0)])
        }
    }
}

//MARK: - API COMMUNICATION
extension MoviesViewController {
    
    func getTrendingMovies(pageNumber: Int) {
        let urlString = NetworkConstants.Enpodints.trendingMovies + String(pageNumber)
        ApiManager.sharedInstance.fetchData(urlString: urlString) { (pagination: Pagination) in
            self.pagination = pagination
            for movie in pagination.movies ?? [] {
                self.movies.append(movie)
            }
            Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: false)
            DispatchQueue.main.async {
                self.moviesCollectionView.reloadData()
            }
        }
    }
    
}
