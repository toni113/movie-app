//
//  MovieDetailsTableViewCell.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 20.2.22.
//

import UIKit

class MovieDetailsTableViewCell: UITableViewCell {
    
    var movieImageView: UIImageView!
    var movieDescriptionLabel: UILabel!
    var genresLabel: UILabel!
    var raitingHolderView: UIView!
    var raitingLabel: UILabel!
    var relaseDateLabel: UILabel!
    var productionCompaniesLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.backgroundColor = .clear
        self.selectionStyle = .none
        
        movieImageView = Utilities.sharedInstance.createImageView(contentMode: .scaleAspectFit)
        
        raitingHolderView = Utilities.sharedInstance.createView(color: .systemBlue, cornerRadius: 10)
        raitingLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 18, weight: .bold), textAligment: .center, textColor: .white)
        
        genresLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 16, weight: .regular), textAligment: .left, textColor: .gray)
        
        relaseDateLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 16, weight: .regular), textAligment: .left, textColor: .black)
        
        movieDescriptionLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 16, weight: .regular), textAligment: .left, textColor: .black)
        
        productionCompaniesLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 16, weight: .regular), textAligment: .left, textColor: .gray)
        
        self.contentView.addSubview(movieImageView)
        self.contentView.addSubview(raitingHolderView)
        self.contentView.addSubview(raitingLabel)
        self.contentView.addSubview(genresLabel)
        self.contentView.addSubview(relaseDateLabel)
        self.contentView.addSubview(movieDescriptionLabel)
        self.contentView.addSubview(productionCompaniesLabel)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        if Utilities.sharedInstance.isLandscape() {
            movieImageView.snp.makeConstraints { make in
                make.left.equalTo(self.contentView).offset(10)
                make.centerY.equalTo(self.contentView)
                make.width.equalTo(self.contentView).dividedBy(2.5)
                make.height.equalTo(self.movieImageView.snp.width).dividedBy(1.77)
            }
            
            raitingLabel.snp.makeConstraints { make in
                make.edges.equalTo(raitingHolderView).inset(10)
            }
            
            raitingHolderView.snp.makeConstraints { make in
                make.right.top.equalTo(movieImageView).inset(20)
            }
            
            genresLabel.snp.makeConstraints { make in
                make.left.equalTo(self.movieImageView.snp.right).offset(10)
                make.right.equalTo(self.contentView).offset(-10)
                make.top.equalTo(self.contentView).offset(20)
            }
            
            relaseDateLabel.snp.makeConstraints { make in
                make.top.equalTo(genresLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
            }
            
            movieDescriptionLabel.snp.makeConstraints { make in
                make.top.equalTo(relaseDateLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
            }
            
            productionCompaniesLabel.snp.makeConstraints { make in
                make.top.equalTo(movieDescriptionLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
                make.bottom.lessThanOrEqualTo(self.contentView).offset(-20)
            }
        } else {
            movieImageView.snp.makeConstraints { make in
                make.top.left.right.equalTo(self.contentView)
                make.height.equalTo(self.contentView.snp.width).dividedBy(1.77)
            }
            
            raitingLabel.snp.makeConstraints { make in
                make.edges.equalTo(raitingHolderView).inset(10)
            }
            
            raitingHolderView.snp.makeConstraints { make in
                make.right.top.equalTo(movieImageView).inset(20)
            }
            
            genresLabel.snp.makeConstraints { make in
                make.left.right.equalTo(self.contentView).inset(10)
                make.top.equalTo(movieImageView.snp.bottom).offset(10)
            }
            
            relaseDateLabel.snp.makeConstraints { make in
                make.top.equalTo(genresLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
            }
            
            movieDescriptionLabel.snp.makeConstraints { make in
                make.top.equalTo(relaseDateLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
            }
            
            productionCompaniesLabel.snp.makeConstraints { make in
                make.top.equalTo(movieDescriptionLabel.snp.bottom).offset(20)
                make.left.right.equalTo(genresLabel)
                make.bottom.lessThanOrEqualTo(self.contentView).offset(-20)
            }
        }
    }
    
    //MARK: - SETUP CELL
    func setupCell(movie: Movie) {
        var genresText = ""
        for genre in movie.genres ?? [] {
            if genresText != "" {
                genresText += ", " + (genre.name ?? "")
            } else {
                genresText += " " + (genre.name ?? "")
            }
        }
        genresLabel.text = "Genres: " + genresText
        
        var productionCompnaiesText = ""
        for productionCompany in movie.production_companies ?? [] {
            if productionCompnaiesText != "" {
                genresText += ", " + (productionCompany.name ?? "")
            } else {
                productionCompnaiesText += " " + (productionCompany.name ?? "")
            }
        }
        productionCompaniesLabel.text = "Production Companies: " + productionCompnaiesText
        
        relaseDateLabel.text = (movie.status ?? "") + " \n" + (movie.release_date ?? "")
        raitingLabel.text = String(movie.vote_average ?? 0.0)
        movieDescriptionLabel.text = movie.overview ?? ""
        movieImageView.downloadImageFrom(url: NetworkConstants.imageBaseUrl + (movie.backdrop_path ?? ""))
        remakeConstraints()
    }

    //MARK: - REMAKE CONSTRAINTS FOR ORIENTATION CHANGE
    func remakeConstraints() {
        if Utilities.sharedInstance.isLandscape() {
            movieImageView.snp.remakeConstraints { make in
                make.left.equalTo(self.contentView).offset(10)
                make.centerY.equalTo(self.contentView)
                make.width.equalTo(self.contentView).dividedBy(2.5)
                make.height.equalTo(self.movieImageView.snp.width).dividedBy(1.77)
            }
            
            genresLabel.snp.remakeConstraints { make in
                make.left.equalTo(self.movieImageView.snp.right).offset(10)
                make.right.equalTo(self.contentView).offset(-10)
                make.top.equalTo(self.contentView).offset(20)
            }
        } else {
            movieImageView.snp.remakeConstraints { make in
                make.top.left.right.equalTo(self.contentView)
                make.height.equalTo(self.contentView.snp.width).dividedBy(1.77)
            }
            
            genresLabel.snp.remakeConstraints { make in
                make.left.right.equalTo(self.contentView).inset(10)
                make.top.equalTo(movieImageView.snp.bottom).offset(10)
            }
        }
    }
}
