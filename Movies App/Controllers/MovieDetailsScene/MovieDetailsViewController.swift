//
//  MovieDetailsViewController.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 20.2.22.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer

class MovieDetailsViewController: UIViewController {
    
    var tableView: UITableView!
    var movie: Movie!
    var videos = [Video]()
    
    init(movie: Movie) {
        super.init(nibName: nil, bundle: nil)
        self.movie = movie
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupConstraints()
        Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: true)
        getMovieDetails()
        
        NotificationCenter.default.addObserver(self, selector: #selector(getOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    @objc func getOrientation() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.view.backgroundColor = .systemGray6
        
        self.title = movie.original_title ?? ""
        
        tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
        tableView.register(MovieDetailsTableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.view.addSubview(tableView)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        tableView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.left.equalTo(self.view.safeAreaLayoutGuide.snp.left)
            make.right.equalTo(self.view.safeAreaLayoutGuide.snp.right)
        }
    }
}

//MARK: - TABLE VIEW DELEGATE AND DATASOURCE
extension MovieDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MovieDetailsTableViewCell
        cell.setupCell(movie: movie)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: API COMMUNICATION
extension MovieDetailsViewController {
    
    func getMovieDetails() {
        let urlString = NetworkConstants.Enpodints.movieDetails + String(movie.id ?? 0) + NetworkConstants.api_key
        ApiManager.sharedInstance.fetchData(urlString: urlString) { (movie: Movie) in
            self.movie = movie
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            Utilities.sharedInstance.showLoader(vc: self, shouldAnimate: false)
        }
    }
}
