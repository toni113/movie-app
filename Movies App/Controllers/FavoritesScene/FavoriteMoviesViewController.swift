//
//  FavoriteMoviesViewController.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import UIKit

class FavoriteMoviesViewController: UIViewController {

    var collectionView: UICollectionView!
    var noDataLabel: UILabel!
    var movies = [Movie]()
    
    //Helpers
    var numberOfCellsHorizontal: CGFloat = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getOrientation()
        getMoviesData()
        setupViews()
        setupConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(getOrientation), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    //MARK: - GET MOVIES DATA
    func getMoviesData() {
        self.movies = UserPersistence.sharedInstance.getFavoriteMovies()
    }
   
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.view.backgroundColor = .systemGray6
        
        self.title = "Favorites"
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(MovieCollectionViewCell.self, forCellWithReuseIdentifier: "movieCell")
        
        noDataLabel = Utilities.sharedInstance.createLabel(text: "Your favorites list is empty.", font: UIFont.systemFont(ofSize: 14), textAligment: .center, textColor: .black)
        
        if movies.count == 0 {
            noDataLabel.isHidden = false
        } else {
            noDataLabel.isHidden = true
        }
        
        self.view.addSubview(collectionView)
        self.view.addSubview(noDataLabel)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.left.equalTo(self.view.safeAreaLayoutGuide.snp.left).inset(10)
            make.right.equalTo(self.view.safeAreaLayoutGuide.snp.right).inset(10)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
        }
        
        noDataLabel.snp.makeConstraints { make in
            make.centerY.equalTo(collectionView)
            make.left.right.equalTo(self.view).inset(30)
        }
    }
    
    //MARK: - ORIENTATION CHANGED
    @objc func getOrientation() {
        if Utilities.sharedInstance.isLandscape() {
            numberOfCellsHorizontal = 4
        } else {
            numberOfCellsHorizontal = 2
        }
        
        if let collectionView = collectionView {
            collectionView.reloadData()
        }
    }
}

//MARK: - COLLECTION VIEW DELEGATE AND DATASOURCE
extension FavoriteMoviesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.delegate = self
        cell.setupCell(movie: movies[indexPath.row], index: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let window = UIApplication.shared.windows.first
        let leftPadding: CGFloat = window?.safeAreaInsets.left ?? 0
        let rigthPadding: CGFloat = window?.safeAreaInsets.right ?? 0
        let safeAreaSize = leftPadding + rigthPadding
        let widht = (self.view.frame.size.width - (numberOfCellsHorizontal * 10) - 10 - safeAreaSize) / numberOfCellsHorizontal
        return CGSize(width: widht , height: widht * 1.5)    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(MovieDetailsViewController(movie: movies[indexPath.row]), animated: true)
    }
}

//MARK: - MOVIE COLLECTION VIEW CELL DELEGATE
extension FavoriteMoviesViewController: MovieCollectionViewCellDelegate {
    func favoriteButtonAction(sender: UIButton) {
        if movies.count > sender.tag {
            DispatchQueue.main.async {
                self.movies.remove(at: sender.tag)
                if self.movies.count == 0 {
                    self.noDataLabel.isHidden = false
                } else {
                    self.noDataLabel.isHidden = true
                }
                UserPersistence.sharedInstance.addToFavorite(favoriteMovies: self.movies)
                self.collectionView.reloadData()
            }
        }
    }
}
