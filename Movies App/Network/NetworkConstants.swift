//
//  NetworkConstants.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import Foundation

struct NetworkConstants {
    
    static let api_key = "?api_key=011204a18e2cd5e9be28eb2526672e32"
    static let baseApiUrl = "https://api.themoviedb.org/3"
    static let imageBaseUrl = "https://image.tmdb.org/t/p/w500"
    
    struct Enpodints {
        static let trendingMovies = NetworkConstants.baseApiUrl + "/trending/movie/week" + NetworkConstants.api_key + "&page="
        static let movieDetails = NetworkConstants.baseApiUrl + "/movie/"
        static let searchMovies = NetworkConstants.baseApiUrl + "/search/movie" + NetworkConstants.api_key + "&query="
        static let searchTvShows = NetworkConstants.baseApiUrl + "/search/tv" + NetworkConstants.api_key + "&query="
    }
}
