//
//  ApiManager.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import Foundation

//typealias CompletitionCallBack = ((_ success: Bool, _ responseObject: [String:Any]?)-> ())?

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    func fetchData<T: Codable>(urlString: String, completition: @escaping (T) -> ()) {
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!) { data, response, error in
            
            if let error = error {
                print("Failed to fetch data", error)
                return
            }
            
            guard let data = data else { return }
            
            do {
                let obj = try JSONDecoder().decode(T.self, from: data)
                completition(obj)
            } catch let jsonErr {
                print("Failed to decode json:", jsonErr)
            }
        }.resume()
    }
}
