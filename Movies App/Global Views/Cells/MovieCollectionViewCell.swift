//
//  MovieCollectionViewCell.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import UIKit

protocol MovieCollectionViewCellDelegate {
    func favoriteButtonAction(sender: UIButton)
}

class MovieCollectionViewCell: UICollectionViewCell {
    
    var backgroundImageView: UIImageView!
    var textHolderView: UIView!
    var movieTitleLabel: UILabel!
    var movieOverviewLabel: UILabel!
    var favoriteButton: UIButton!
    
    var delegate: MovieCollectionViewCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.backgroundImageView.image = nil
    }
    
    //MARK: - SETUP VIEWS
    func setupViews() {
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        backgroundImageView = Utilities.sharedInstance.createImageView(contentMode: .scaleAspectFit)
        
        textHolderView = Utilities.sharedInstance.createView(color: .black.withAlphaComponent(0.65), cornerRadius: 10)
        
        movieTitleLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 16, weight: .semibold), textAligment: .left, textColor: .white)
        movieTitleLabel.numberOfLines = 0
        movieOverviewLabel = Utilities.sharedInstance.createLabel(text: "", font: UIFont.systemFont(ofSize: 14, weight: .regular), textAligment: .left, textColor: .white)
        movieOverviewLabel.numberOfLines = 2
        
        favoriteButton = Utilities.sharedInstance.createButton(title: "", backgorundColor: .yellow, textColor:.clear, imageName: "favorite-inactive")
        favoriteButton.addTarget(self, action: #selector(favoriteButtonTapped(sender:)), for: .touchUpInside)
        
        self.contentView.addSubview(backgroundImageView)
        self.contentView.addSubview(textHolderView)
        self.contentView.addSubview(movieTitleLabel)
        self.contentView.addSubview(movieOverviewLabel)
        self.contentView.addSubview(favoriteButton)
    }
    
    //MARK: - SETUP CONSTRAINTS
    func setupConstraints() {
        backgroundImageView.snp.makeConstraints { make in
            make.edges.equalTo(self.contentView)
        }
        
        movieOverviewLabel.snp.makeConstraints { make in
            make.bottom.left.right.equalTo(self.textHolderView).inset(10)
        }
        
        movieTitleLabel.snp.makeConstraints { make in
            make.bottom.equalTo(movieOverviewLabel.snp.top).offset(-10)
            make.left.right.equalTo(movieOverviewLabel)
        }
        
        textHolderView.snp.makeConstraints { make in
            make.bottom.right.left.equalTo(self.contentView).inset(10)
            make.top.equalTo(movieTitleLabel).offset(-10)
        }
        
        favoriteButton.snp.makeConstraints { make in
            make.top.right.equalTo(self.contentView).inset(15)
            make.width.height.equalTo(35)
        }
    }
    
    //MARK: - FAVORITE BUTTON ACTION
    @objc func favoriteButtonTapped(sender: UIButton) {
        delegate?.favoriteButtonAction(sender: sender)
    }
    
    //MARK: - SETUP CELL
    func setupCell(movie: Movie, index: Int) {
        favoriteButton.tag = index
        movieTitleLabel.text = movie.original_title ?? ""
        movieOverviewLabel.text = movie.overview ?? ""
        backgroundImageView.downloadImageFrom(url: NetworkConstants.imageBaseUrl + (movie.poster_path ?? ""))
        
        let favMovies = UserPersistence.sharedInstance.getFavoriteMovies()
        if favMovies.contains(where: {$0.id == movie.id}) {
            favoriteButton.setBackgroundImage(UIImage(named: "favorite-active"), for: .normal)
        } else {
            favoriteButton.setBackgroundImage(UIImage(named: "favorite-inactive"), for: .normal)
        }
    }
}
