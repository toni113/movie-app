//
//  UserPersistence.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 21.2.22.
//

import Foundation


class UserPersistence {
    
    static let sharedInstance = UserPersistence()
    
    let defaults = UserDefaults.standard
    
    func addToFavorite(favoriteMovies: [Movie]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(favoriteMovies) {
            defaults.set(encoded, forKey: "favoriteMovies")
        }
    }
    
    func getFavoriteMovies() -> [Movie] {
        if let objects = defaults.value(forKey: "favoriteMovies") as? Data {
            let decoder = JSONDecoder()
            if let objectsDecoded = try? decoder.decode(Array.self, from: objects) as [Movie] {
                return objectsDecoded
            }
        } else {
            return []
        }
        return []
    }
    
}
