//
//  Utilities.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 20.2.22.
//

import Foundation
import UIKit

class Utilities {
    
    static let sharedInstance = Utilities()
    
    let activityIndicator = UIActivityIndicatorView(style: .large)
    
    //MARK: - SETUP LOADER
    func showLoader(vc: UIViewController, shouldAnimate: Bool){
        DispatchQueue.main.async {
            self.activityIndicator.color = .black
            vc.view.addSubview(self.activityIndicator)
            
            self.activityIndicator.snp.makeConstraints { (make) in
                make.center.equalTo(vc.view)
            }
            
            if(shouldAnimate){
                self.activityIndicator.startAnimating()
            } else {
                self.activityIndicator.stopAnimating()
            }
        }
    }
    func jsonToData(json: Any) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
    
    //MARK: - CREATE LABEL
    func createLabel(text: String, font: UIFont, textAligment: NSTextAlignment, textColor: UIColor) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = textAligment
        label.text = text
        label.font = font
        label.textColor = textColor
        
        return label
    }
    
    //MARK: - CREATE IMAGE VIEW
    func createImageView(contentMode: UIView.ContentMode) -> UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = contentMode
        
        return imageView
    }
    
    //MARK: - CREATE VIEW
    func createView(color: UIColor, cornerRadius: CGFloat) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        view.layer.cornerRadius = cornerRadius
        
        return view
    }
    
    //MARK: - CREATE BUTTON
    func createButton(title: String, backgorundColor: UIColor, textColor: UIColor, imageName: String) -> UIButton {
        let button = UIButton()
        if imageName == "" {
            button.setTitle(title, for: .normal)
            button.backgroundColor = backgorundColor
            button.setTitleColor(textColor, for: .normal)
            button.layer.cornerRadius = 10
        } else {
            button.setBackgroundImage(UIImage(named: imageName), for: .normal)
        }
        
        return button
    }
    
    //MARK: - IS LANDSCAPE
    func isLandscape() -> Bool {
        if UIDevice.current.orientation.isLandscape {
            return true
        } else {
            return false
        }
    }
    
    //MARK: - IS IPAD
    func isIPad() -> Bool {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return true
        } else {
            return false
        }
    }
}
