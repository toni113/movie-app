//
//  Extension.swift
//  Movies App
//
//  Created by Antonio Cvetkovski on 19.2.22.
//

import Foundation
import UIKit

let imageCache = NSCache<NSString, AnyObject>()

//MARK: - EXTENSION FOR UIIMAGE VIEW FOR GETTING IMAGE FROM URL
extension UIImageView {
    
    func downloadImageFrom(url: String) {
        guard let url = URL(string: url) else { return }
        
        image = nil
        
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) as? UIImage {
            self.image = cachedImage
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data)
                    if let imageToCache = imageToCache {
                        imageCache.setObject(imageToCache, forKey: url.absoluteString as NSString)
                        self.image = imageToCache
                    }
                }
            }.resume()
        }
    }

}
